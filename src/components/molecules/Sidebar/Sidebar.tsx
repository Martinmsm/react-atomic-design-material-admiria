import React, { memo } from 'react';
import Div from "../../atoms/View/Div";
import LeftMenuLogo from "../LeftMenuLogo/LeftMenuLogo";
import LeftMenuList from "../LeftMenuList/LeftMenuList";


function Sidebar() {
    return (
        <Div className="left side-menu">
            <LeftMenuLogo/>
            <LeftMenuList/>
        </Div>
    )
}

export default memo(Sidebar)