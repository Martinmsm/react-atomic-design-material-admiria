import React, {useState, useEffect, memo} from 'react';
import Header from "../../atoms/Header/Header";
import Div from "../../atoms/View/Div";
import Span from "../../atoms/View/Span";
import A from "../../atoms/A/A";


function Heading() {
    return (
        <Header className="Header">
            <Div className="header-title">
                <i className="fas fa-chevron-circle-left"></i>
                <Span>&nbsp;Server /</Span>
                <Span> Server</Span>
            </Div>
            <Div className="header-menu text-right">
                <A className="notify">
                    <Span className="notify-number">16</Span>
                    <i className="fas fa-bell"></i>
                </A>
                <A href="" className="message mgl-20">
                    <i className="far fa-comment-alt"></i>
                </A>
                <A href="" className="edit mgl-20">
                    <i className="fa fa-pencil"></i>
                </A>
                <A href="" className="list mgl-20">
                    <i className="far fa-edit"></i>
                </A>
                <Div className="account text-right mgl-20 mgr-30">
                    <Div className="user">
                        <A href="">
                            <i className="fa fa-user"></i>
                        </A>
                    </Div>
                </Div>
            </Div>
            <hr/>
        </Header>
    )
}

export default memo(Heading)