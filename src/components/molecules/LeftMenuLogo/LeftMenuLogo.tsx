import React, { memo } from 'react';
import Div from "../../atoms/View/Div";
import A from "../../atoms/A/A";
import Image from "../../atoms/Image/Image";
import P from "../../atoms/View/P";


function LeftMenuLogo() {
    return (
        <Div className="topbar-left">
            <Div className="">
                <A href="#" className="logo"><Image className="logo-image" src="resources/assets/images/logo/m_logo_1.png" alt="logo"/></A>
                <P className="logo-text">드론 영상 보관 및 처리 시스템</P>
            </Div>
        </Div>
    )
}

export default memo(LeftMenuLogo)