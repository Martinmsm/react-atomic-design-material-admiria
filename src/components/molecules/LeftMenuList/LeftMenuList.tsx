import React, { memo } from 'react';
import Div from "../../atoms/View/Div";
import A from "../../atoms/A/A";
import Span from "../../atoms/View/Span";

function LeftMenuList() {
    return (
        <Div className="sidebar-inner">
            <div className="sidebar-menu">
                <ul>
                    <li className="has_sub nav-active">
                        <A className="sidebar-anchor" href="#"><Span> Account <Span className="pull-right">
                            <i className="mdi mdi-chevron-right"></i></Span> </Span></A>
                    </li>
                    <li className="has_sub">
                        <A className="sidebar-anchor" href="#"><Span> Groups <Span className="pull-right">
                            <i className="mdi mdi-chevron-right"></i></Span> </Span></A>
                    </li>
                    <li className="has_sub">
                        <A className="sidebar-anchor" href="#"><Span> 시스템 로그 <Span className="pull-right">
                            <i className="mdi mdi-chevron-right"></i></Span> </Span></A>
                    </li>
                    <li className="has_sub">
                        <A className="sidebar-anchor" href="#"><Span> 시스템 통계 <Span className="pull-right">
                            <i className="mdi mdi-chevron-right"></i></Span> </Span></A>
                    </li>
                </ul>
            </div>
        </Div>
    )
}

export default memo(LeftMenuList)