import React, {memo} from "react";
import ContainerContent from "../../molecules/ContainerContent/ContainerContent";

function Container() {
    return (
        <ContainerContent/>
    )
}

export default memo(Container)