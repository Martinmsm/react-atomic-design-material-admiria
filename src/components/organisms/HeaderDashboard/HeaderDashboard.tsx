import React, {memo} from "react";
import Heading from "../../molecules/Heading/Heading";

function HeaderDashboard() {
    return (
        <Heading/>
    )
}

export default memo(HeaderDashboard)