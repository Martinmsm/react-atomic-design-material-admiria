import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface SectionProps {
    children?: React.ReactNode;
    flex?: number | 'auto';
    className?: string;

    [prop: string]: any;

    bgColor?: string;
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledSection = styled.section<SectionProps>`
`;

const Section = ({
                  children,
                  flex = 'auto',
                  className,
                  bgColor
              }: SectionProps) => {
    const classCandidate = [className];
    const commonProps = {
        flex,
        bgColor
    };
    return <StyledSection {...commonProps} className={cn(classCandidate)}>
        {children}
    </StyledSection>
};

export default memo(Section);
