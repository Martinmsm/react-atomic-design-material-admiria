import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface DivProps {
    children?: React.ReactNode;
    className?: string;

    [prop: string]: any;

    bgColor?: string;
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledDiv = styled.div<DivProps>`
  &.wrapper {height: 100%; overflow: hidden;width: 100%;}
  &.left { bottom: 50px; height: 100%; margin-bottom: -70px; margin-top: 0; padding-bottom: 70px; position: fixed;}
  &.side-menu {bottom: 50px; height: 100%; margin-bottom: -70px; margin-top: 0; padding-bottom: 70px; position: fixed;}
  &.topbar-left {padding-left: 30px;}
  &.sidebar-inner {height: 100%;}
  &.sidebar-menu {background-color: #121b26; padding-bottom: 50px; width: 100%;}
  &.nav-active {background-color: #0b131c;color: rgba(255, 255, 255, 0.8);
  }
  &.sidebar-menu > ul > li > a > span {
    vertical-align: middle;
  }

  &.sidebar-menu > ul > li > a > i {
    display: inline-block;
    font-size: 18px;
    line-height: 17px;
    margin-left: 3px;
    margin-right: 10px;
    text-align: center;
    vertical-align: middle;
    width: 20px;
  }

  &.sidebar-menu > ul > li > a > i.i-right {
    float: right;
    margin: 3px 0 0 0;
  }

  &.sidebar-menu > ul > li > a.active {
    color: rgba(255, 255, 255, 0.8);
  }

  &.sidebar-menu > ul > li > a.active i {
    color: rgba(255, 255, 255, 0.8);
  }

  &.sidebar-menu > ul > li.nav-active > ul {
    display: block;
  }
`;

const Div = ({
                  children,
                  className,
                  bgColor
              }: DivProps) => {
    const classCandidate = [className];
    const commonProps = {
        bgColor
    };
    return <StyledDiv {...commonProps} className={cn(classCandidate)}>
            {children}
        </StyledDiv>
};

export default memo(Div);
