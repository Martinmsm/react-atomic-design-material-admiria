import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface PProps {
    children?: React.ReactNode;
    className?: string;

    [prop: string]: any;

    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledP = styled.p<PProps>`
    &.logo-text {color: white;font-size: 14px}
`;

const P = ({
               children,
               className,
           }: PProps) => {
    const classCandidate = [className];
    const commonProps = {
    };
    return <StyledP {...commonProps} className={cn(classCandidate)}>
        {children}
    </StyledP>
};

export default memo(P);
