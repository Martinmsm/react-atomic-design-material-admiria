import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface SpanProps {
    children?: React.ReactNode;
    className?: string;
}

const StyledSpan = styled.span<SpanProps>`
`;

const Span = ({
                 children,
                 className,
             }: SpanProps) => {
    const classCandidate = [className];
    const commonProps = {
    };
    return <StyledSpan {...commonProps} className={cn(classCandidate)}>
        {children}
    </StyledSpan>
};

export default memo(Span);
