import React, { memo } from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface ULProps {
    children?: React.ReactNode;
    className?: string;

    [prop: string]: any;

    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledUL = styled.ul<ULProps>`
  
`;

const UL = ({
                  children,
                  className,
              }: ULProps) => {
    const classCandidate = [className];
    const commonProps = {
    };
    return <StyledUL {...commonProps} className={cn(classCandidate)}>
            {children}
        </StyledUL>
};

export default memo(UL);
