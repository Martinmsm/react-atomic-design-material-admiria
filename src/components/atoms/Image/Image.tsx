import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface ImageProps {
    className?: string;
    src: string,
    alt: string,
}

const StyledImage = styled.img<ImageProps>`
    &.logo-image {width: 176px; height: 36px}
`;

const Image = ({
                   className,
                   src,
                   alt
               }: ImageProps) => {
    const classCandidate = [className];
    const commonProps = {};
    return <StyledImage {...commonProps} className={cn(classCandidate)} src={src} alt={alt}>
    </StyledImage>
};

export default memo(Image);