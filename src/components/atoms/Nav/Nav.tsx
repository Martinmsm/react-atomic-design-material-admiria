import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface NavProps {
    children?: React.ReactNode;
    className?: string;

    [prop: string]: any;

    bgColor?: string;
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledNav = styled.nav<NavProps>`
`;

const Nav = ({
                  children,
                  className,
                  bgColor
              }: NavProps) => {
    const classCandidate = [className];
    const commonProps = {
        bgColor
    };
    return <StyledNav {...commonProps} className={cn(classCandidate)}>
            {children}
        </StyledNav>
};

export default memo(Nav);
