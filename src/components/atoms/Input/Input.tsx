import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface InputProps {
    className: string;
    type: string;
    name: string;
    id: string;
    value: any;
}

const StyledInput = styled.img<InputProps>`

`;

const Input = ({
                   className,
                   type,
                   name,
                   id,
                   value
               }: InputProps) => {
    const classCandidate = [className];
    const commonProps = {};
    return <StyledInput {...commonProps} className={cn(classCandidate)} type={type} name={name} id={id} value={value}>
    </StyledInput>
};

export default memo(Input);