import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface AProps {
    children?: React.ReactNode;
    className?: string;
    url?: string;

    [prop: string]: any;

    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledA = styled.a<AProps>`
  cursor: pointer;

  &.sidebar-anchor {
    color: rgba(255, 255, 255, 0.5) !important;
    display: block;
    padding: 12px 20px;
    background-color: #121b26;
    border: 0;
    font-weight: normal;
    line-height: 1;
    list-style: none;
    margin: 0;
    position: relative;
    text-decoration: none;
  }

  &.sidebar-anchor:hover {
    color: rgba(255, 255, 255, 0.8) !important;
    text-decoration: none;
  }
`;

const A = ({
               children,
               className,
           }: AProps) => {
    const classCandidate = [className];
    const commonProps = {};
    return <StyledA {...commonProps} className={cn(classCandidate)}>
        {children}
    </StyledA>
};

export default memo(A);
