import React, { memo } from 'react';
import styled from 'styled-components';
import cn from 'classnames';
import { Link } from 'react-router-dom';

interface ButtonProps {
    children?: React.ReactNode;
    flex?: number | 'auto';
    color?: string;
    size?: 'small' | 'normal' | 'big';
    type?: 'link' | 'button' | 'submit';
    url?: string;
    outline?: string;
    bgColor?: string;
    transparent?: boolean;
    className?: string;
    [prop: string]: any;
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledButton = styled.button<ButtonProps>`
	flex: ${(props: ButtonProps) => props.flex};
	display: flex;
	justify-content: center;
	align-items: stretch;
	border-radius: 30px;
	border: ${(props: ButtonProps) => (props.outline === 'none' ? 'none' : `0.7px solid ${props.outline}`)};
	background: ${(props: ButtonProps) => (props.transparent ? 'transparent' : props.bgColor)};
	color: ${(props: ButtonProps) => props.color};
	cursor: pointer;
	outline: none;
	width: 100%;
	border-radius: 30px;
	line-height: 40px;
	font-family: 'NotoSansKR-Medium';
	align-items: center;
	display: flex;
	&.small {
		font-size: 15px;
		height:32px
	}
	&.normal {
	    border-radius: 30px;
		line-height: 40px;
		font-family: 'NotoSansKR-Medium';
		font-size: 15px;
		color: #fff;
}
	}
	&.big {
		padding: 14px 14px;
		font-size: 1.4rem;
	}
`;

const StyledLink = styled(Link)`
	display: flex;
	align-items: center;
	text-decoration: none;
	color: black;
`;

const Button = ({
                    children,
                    flex = 'auto',
                    color = 'black',
                    outline = 'black',
                    bgColor = 'white',
                    transparent = false,
                    size = 'normal',
                    type = 'button',
                    url,
                    className,
                    onClick
                }: ButtonProps) => {
    const classCandidate = [size, className];
    const commonProps = {
        flex,
        color,
        size,
        outline,
        bgColor,
        transparent
    };

    const RealButton = (
        <StyledButton {...commonProps} className={cn(classCandidate)} onClick={onClick}>
            {children}
        </StyledButton>
    );
    const RealLink = (
        <StyledLink to={url!}>
            <StyledButton {...commonProps} className={cn(classCandidate)}>
                {children}
            </StyledButton>
        </StyledLink>
    );

    return type === 'link' && url ? RealLink : RealButton;
};

export default memo(Button);