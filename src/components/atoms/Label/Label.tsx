import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface LabelProps {
    children: string;
    className?: string;
    htmlFor: any
}

const StyledLabel = styled.label<LabelProps>`
  &.mgr-66 {margin-right: 66px}
  &.mgr-68 {margin-right: 68px}
  &.mgr-92 {margin-right: 92px}
  &.mgr-70 {margin-right: 70px}
  &.mgr-48 {margin-right: 48px}
  &.mgr-82 {margin-right: 82px}
  &.mgr-59 {margin-right: 59px}
  &.normal {}
`;

const Label = ({
                   children,
                   className,
                   htmlFor
               }: LabelProps) => {
    const classCandidate = [className];
    const commonProps = {};
    return <StyledLabel {...commonProps} className={cn(classCandidate)} htmlFor={htmlFor}>
        {children}
    </StyledLabel>
};

export default memo(Label);