import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface TRProps {
    children?: React.ReactNode;
    flex?: number | 'auto';
    className?: string;

    [prop: string]: any;

    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledTR = styled.tr<TRProps>`
    
`;

const TR = ({
                children,
                flex = 'auto',
                className,
            }: TRProps) => {
    const classCandidate = [className];
    const commonProps = {
        flex,
    };
    return <StyledTR {...commonProps} className={cn(classCandidate)}>
        {children}
    </StyledTR>
};

export default memo(TR);
